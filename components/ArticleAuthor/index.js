import ResponsiveImage from '../../utilities/responsiveImage'

const ArticleAuthor = (props) => {
  const { author } = props

  return (
    <div className='article-author'>
      <ResponsiveImage
        customClass='article-author-image'
        small={author.images.small}
        medium={author.images.medium}
        large={author.images.large}
        alt={author.name}
      />
      <p className='article-author-name'>{author.name}</p>
    </div>
  )
}

export default ArticleAuthor
