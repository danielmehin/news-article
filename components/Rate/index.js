import { useState } from 'react'

const Rate = (props) => {
  const [rate, setRate] = useState(0)
  const [newRate, setNewRate] = useState(false)
  const values = [1,2,3,4,5]

  const handleSubmit = (event) => {
    event.preventDefault()
    setNewRate(true)
  }

  const handleClose = (event) => {
    event.preventDefault()
    props.handleClick()
  }

  return (
    newRate
      ?
        <div className='rate-submitted'>
          <p>Thank you. overall rating: {rate}</p>
          <button
            className='btn primary-btn'
            type='submit'
            onClick={(event) => handleClose(event)}>Close</button>
        </div>
      :
        <form className='rate'>
          <div className='rate-numbers'>
          {values.map(item => <label key={item} className='rate-star'>
              <input
                type='radio'
                name='rating'
                value={item}
                onChange={() => setRate(item)}
              />
              <svg className='svg' width='24' height='24' viewBox='0 0 24 24'>
                <path d='M12 .587l3.668 7.568 8.332 1.151-6.064 5.828 1.48 8.279-7.416-3.967-7.417 3.967 1.481-8.279-6.064-5.828 8.332-1.151z'/>
              </svg>
            </label>
          )}
          <button
            className='btn success-btn'
            type='submit'
            onClick={(event) => handleSubmit(event)}>Submit</button>
          </div>
        </form>
  )
}

export default Rate
