import ResponsiveImage from '../../utilities/responsiveImage'

const ArticleCard = (props) => {
  const { data }  = props

  return (
    <div className={`${data.banner ? 'article-banner' : 'article-card'}`}>
      <ResponsiveImage
        customClass='article-image'
        small={data.images.small}
        medium={data.images.medium}
        large={data.images.large}
        alt={data.title}
      >
      </ResponsiveImage>
      {
        data.banner
        ? <h1>{data.title}</h1>
        : <h2>{data.title}</h2>
      }
      <p>{data.summary}</p>
    </div>
  )
}

export default ArticleCard
