import Link from 'next/link'

const Header = () => (
  <header className='header'>
    <img
      className='header-logo'
      src='./assets/logo.png'
      alt='loup'
    />
  </header>
)

export default Header
