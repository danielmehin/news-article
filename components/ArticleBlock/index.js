import ArticleCard from '../ArticleCard'

const ArticleBlock = (props) => {
  const { data } = props

  return (
      Array.isArray(data) !== true
      ?
        <div dangerouslySetInnerHTML={{ __html: data }} />
      :
        data.map((item, index) => <ArticleCard
          key={index}
          data={{
            banner: false,
            images: {
              small: item.imageList.landscapemobile2x.url,
              medium: item.imageList.landscapetablet2x.url,
              large: item.imageList.landscapedesktop1x.url
            },
            title: item.title,
            summary: item.summary
          }}
        />)

  )
}

export default ArticleBlock
