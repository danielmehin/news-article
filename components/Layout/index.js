import Header from '../Header';

const Layout = props => (
  <div>
    <Header />
    <div className='article'>
      {props.children}
    </div>
  </div>
);

export default Layout
