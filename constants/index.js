const Breakpoints = {
  mobile: '768w',
  tablet: '992w',
  desktop: '1280w'
}

export {
  Breakpoints
}
