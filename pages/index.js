import { useState } from 'react'
import fetch from 'isomorphic-unfetch'
import Layout from '../components/Layout'
import ArticleCard from '../components/ArticleCard'
import ArticleAuthor from '../components/ArticleAuthor'
import ArticleBlock from '../components/ArticleBlock'
import Modal from '../components/Modal'
import Rate from '../components/Rate'

import '../styles/main.scss'

const Index = props => {
  const { article } = props
  const [modalOpen, setModalOpen] = useState(false)

  const handleModal = () => {
    modalOpen ? setModalOpen(false) : setModalOpen(true)
  }

  return (
    <Layout>
      <ArticleCard
        data={{
          banner: true,
          images: {
            small: article.imageList.landscape32medium1x.url,
            medium: article.imageList.landscape32medium2x.url,
            large: article.imageList.landscape32medium3x.url
          },
          title: article.title,
          summary: article.summary
        }}
      />
      {
        article.authors.map((item, index) => <ArticleAuthor
          key={index}
          author={{
            images: {
              small: item.imageList.square1x.url,
              medium: item.imageList.square2x.url,
              large: item.imageList.square3x.url
            },
            name: item.name
          }}
        />)
      }
      {
        article.blocks.map((item, index) => <ArticleBlock key={index} data={item.content} />)
      }
      <a
        className='btn primary-btn'
        onClick={() => handleModal()}>
        Rate this Article
      </a>
      {
        modalOpen
        &&
          <Modal
            title='Please rate this article'
            handleClick={handleModal}
            >
            <Rate handleClick={handleModal} />
          </Modal>
      }
    </Layout>
  )
}

Index.getInitialProps = async function() {
  const res = await fetch('http://localhost:3000/data/article.json')
  const data = await res.json()

  return {
    article: data.result.article
  }
}

export default Index
