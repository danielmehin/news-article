This is a [Next.js](https://nextjs.org/) project bootstrapped with [`create-next-app`](https://github.com/zeit/next.js/tree/canary/packages/create-next-app).

## Getting Started

First, run the development server:

```bash
npm run dev
# or
yarn dev
```

Open [http://localhost:3000](http://localhost:3000) with your browser to see the result.

## Future improvements

My primary focus on this challenge was to display server side rendering with next.js. As a result there are few things that can be improved.

- Open rating modal when user scrolls to end of the page rather than using a 'rate button'
- Use Context and create a state management system to better handle state change for modal and capture rating score
- Writing test cases
- Using CSS in JS instead of classic scss
