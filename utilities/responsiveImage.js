import { Breakpoints } from '../constants'

const ResponsiveImage = (props) => (
  <img
    className={props.customClass}
    src={props.small}
    srcSet={`${props.small} ${Breakpoints.mobile}, ${props.medium} ${Breakpoints.tablet}, ${props.large} ${Breakpoints.desktop}`}
    alt={props.alt ? props.alt : ''}
  />
)

export default ResponsiveImage
